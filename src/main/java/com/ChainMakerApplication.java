package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class ChainMakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChainMakerApplication.class, args);
    }

}
