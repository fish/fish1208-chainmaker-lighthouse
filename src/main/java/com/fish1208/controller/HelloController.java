package com.fish1208.controller;

import com.fish1208.bean.HelloWorld;
import com.fish1208.common.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.sdk.ChainClient;
import org.chainmaker.sdk.SdkException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * hello合约(wasm版)控制器
 */
@Slf4j
@RestController
@RequestMapping("/hello")
public class HelloController {

    private static long rpcCallTimeout = 10000;
    private static long syncResultTimeout = 10000;

    private static final String CONTRACT_NAME = "hello";

    @Autowired
    private ChainClient chainClient;

    @GetMapping(value = "/set")
    public Result<?> set(@RequestParam String n) {
        log.info("执行hello合约set方法...");

        String method = "set";

        Map<String, byte[]> params = new HashMap<>();
        params.put("n",  n.getBytes());

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(CONTRACT_NAME, method, null, params, rpcCallTimeout, syncResultTimeout);
            return Result.data(responseInfo.getTxId());
        } catch (SdkException e) {
            log.error("set is fail, e = {}",e);
        }
        return Result.fail("hello合约set方法失败！");
    }

    @GetMapping(value = "/get")
    public Result<?> get() {
        log.info("执行hello合约get方法...");

        String method = "get";

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            responseInfo = chainClient.queryContract(CONTRACT_NAME, method, null, null, rpcCallTimeout);
            return Result.data(responseInfo.getContractResult().getResult().toString());
        } catch (SdkException e) {
            log.error("get is fail, e = {}",e);
        }
        return Result.fail("hello合约get方法失败！");
    }

}
