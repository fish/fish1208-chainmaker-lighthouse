package com.fish1208.controller;

import com.fish1208.bean.SaveContractDTO;
import com.fish1208.common.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.sdk.ChainClient;
import org.chainmaker.sdk.SdkException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 合约控制器
 */
@Slf4j
@RestController
@RequestMapping("/contract")
public class ContractController {

    private static long rpcCallTimeout = 10000;
    private static long syncResultTimeout = 10000;

    private static final String SAVE_CONTRACT_NAME = "save"; //SaveContract  存证合约，可保存Key-Value及描述信息

    @Autowired
    private ChainClient chainClient;

    @PostMapping(value = "/save/save")
    public Result<?> set(@RequestBody SaveContractDTO dto) {

        String method = "save";

        Map<String, byte[]> params = new HashMap<>();
        params.put("key",  dto.getKey().getBytes());
        params.put("data",  dto.getData().getBytes());
        params.put("desc",  dto.getDesc().getBytes());

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(SAVE_CONTRACT_NAME, method, null, params, rpcCallTimeout, syncResultTimeout);
            return Result.data(responseInfo.getTxId());
        } catch (SdkException e) {
            log.error("save is fail, e = {}",e);
        }
        return Result.fail("SaveContract存证合约 save失败！");
    }

    @GetMapping(value = "/save/find")
    public Result<?> find(@RequestParam String key) {

        String method = "find";

        Map<String, byte[]> params = new HashMap<>();
        params.put("key",  key.getBytes());

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(SAVE_CONTRACT_NAME, method, null, params, rpcCallTimeout, syncResultTimeout);
            return Result.data(responseInfo.getContractResult().getResult().toString());
        } catch (SdkException e) {
            e.printStackTrace();
        }
        return Result.fail("SaveContract存证合约 find失败！");
    }

}
