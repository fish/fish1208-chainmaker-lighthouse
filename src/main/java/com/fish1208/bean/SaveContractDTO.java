package com.fish1208.bean;

import lombok.Data;

@Data
public class SaveContractDTO {

    private String key;

    private String data;

    private String desc;
}
