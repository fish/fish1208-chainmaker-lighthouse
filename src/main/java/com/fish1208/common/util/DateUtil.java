package com.fish1208.common.util;

import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Slf4j
public class DateUtil {

	private DateUtil() {
		// 用于覆盖默认的公共空构造。
	}

	public static final String DATE_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";

	public static String convertTimeToString(Long time) {
		if(null == time) return null;
		return convertTimeToString(time, DATE_TIME_FORMATTER);
	}

	public static String convertTimeToString(Long time, String formatter) {
		DateTimeFormatter ftf = DateTimeFormatter.ofPattern(formatter);
		return ftf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
	}

}
