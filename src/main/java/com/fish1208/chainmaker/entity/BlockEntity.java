package com.fish1208.chainmaker.entity;

import lombok.Data;

import java.math.BigInteger;

@Data
public class BlockEntity {
    private Long blockHeight; //区块序号
    private String blockHash;//区块hash
    private String preBlockHash;//上个区块hash
    private String blockTime;//区块产生时间
}
